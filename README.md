Run full nginx deployments: `ansible-playbook -i hosts 01_run.yaml`

Avalable tags:
- init_nginx - install nginx from apt and setup minimal conf
- cp_html - copies index html file
- setup_firewall - opens only 80 port on server
- setup_nginx_tls_firewall - opens both 80 and 443 ports on the server
- setup_nginx_tls - gen certificates and setup tls config
- copy_ssl_conf - copies ssl config files
- healthcheck - run healthcheck tasks
